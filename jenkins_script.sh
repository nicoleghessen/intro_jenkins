# Install Java with Apt on Ubuntu 20.04

# first update the package
sudo apt update

# check if Java is already installed
java -version

# install the default Java Runtime Environment (JRE), which will install the JRE from OpenJDK 11
sudo apt install default-jre

#verify intstallation
java -version

# You may need the Java Development Kit (JDK) in addition to the JRE in order to compile and run some specific Java-based software. To install the JDK, execute the following command, which will also install the JRE
sudo apt install default-jdk

# verify the JDK is installed by checking Java compiler
javac -version

# you should get an output similar to "javac 11.0.13"

# First, add the repository key to the system:
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -

# append the Debian package repository address to the server’s sources.list:
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'

# run update so that apt will use the new repository.
sudo apt update

# install Jenkins and its dependencies.
sudo apt install jenkins

# start Jenkins by using systemctl
sudo systemctl start jenkins

#use the status command to verify that Jenkins started successfully:
sudo systemctl status jenkins

To set up your installation, visit Jenkins on its default port, 8080, using your server domain name or IP address: http://your_server_ip_or_domain:8080

