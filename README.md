### Prerequisites for installing Jenkins
- One Ubuntu 20.04 server configured with a non-root sudo user and firewall by following the Ubuntu 20.04 initial server setup guide. 

- We recommend starting with at least 1 GB of RAM. Visit Jenkins’s “Hardware Recommendations” for guidance in planning the capacity of a production-level Jenkins installation.

- Oracle JDK 11 installed, following our guidelines on installing specific versions of OpenJDK on Ubuntu 20.04.


# CI Testing 
test number: 1
test number: 2
test number 3
test number 4
test number 5
test number 6
test number 7
test number 8